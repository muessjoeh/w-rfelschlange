import random
import matplotlib.pyplot as plt

SEITEN = 6  # Anzahl der Seiten eines Würfels
RUNDEN = 100  # Anzahl der Runden der dem ersten folgenden Durchgänge
SPIELE = 1000  # Anzahl der gesamten Spiele pro Würfelanzahl


def wuerfelschlange(laenge, runden):
    """
    Berechne die Häufigkeit des erwarteten Ergebnisses bei einer
    Würfelschlange (Methodisch inkorrekt 161)
    laenge: Anzahl der Würfel
    runden: Anzahl der Runden nach dem ersten Durchgang
    return: Anzahl der Durchgänge mit dem erwarteten Ergebnis
    """
    wuerfel = []
    # Würfel der Würfelschlange zufällig befüllen
    for i in range(laenge):
        wuerfel.append(random.randint(1, SEITEN))

    # Erster Durchgang: Durchlaufen
    pos = 0
    while True:
        if pos + wuerfel[pos-1] <= laenge:
            pos += wuerfel[pos-1]
        else:
            break

    # Erster Durchgang: Abschneiden
    bereinigte_wuerfel = wuerfel[:pos]

    # Folgende Durchgänge
    stimmt = 0
    bereinigte_wuerfel[0] = random.randint(1, SEITEN)  # Ersten Würfel nochmal würfeln

    for r in range(runden):
        pos = 0

        while True:
            if pos + bereinigte_wuerfel[pos-1] <= len(bereinigte_wuerfel):
                pos += bereinigte_wuerfel[pos-1]
            else:
                break
        if pos == len(bereinigte_wuerfel):
            stimmt += 1

    return stimmt

werte_x = []
werte_y = []
for wuerfel in range(SEITEN+1,80,3):  # 7..79 in 3er-Schritten
    uebereinstimmend = 0
    for durchgang in range(SPIELE):
        uebereinstimmend += wuerfelschlange(wuerfel, RUNDEN)
    werte_x.append(wuerfel)
    werte_y.append(uebereinstimmend / RUNDEN / SPIELE)


fig, ax = plt.subplots()
ax.plot(werte_x, werte_y)
ax.set_xlabel("Zahl der Würfel")
ax.set_ylabel("Relative Häufigkeit des vorgesehenen Ablaufs")
plt.grid(b=True, which='major', color='#666666', linestyle='-')
plt.grid(b=True, which='minor', color='#888888', linestyle='-', alpha=0.4)
plt.minorticks_on()
plt.show()
